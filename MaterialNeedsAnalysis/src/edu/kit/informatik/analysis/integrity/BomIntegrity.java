/**
 * 
 */
package edu.kit.informatik.analysis.integrity;

import java.util.List;
import edu.kit.informatik.analysis.Unit;

/**
 * Provides methods to ensure the integrity of the material needs analysis system
 * 
 * Implemented as interface to easily change implementation of the integrity operations
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public interface BomIntegrity {
    /**
     * Checks if adding the units of {@code toAddList} to unit {@code toAdd} 
     * would create a cycle in the product structure.
     * 
     * @param toAddTo unit to which to add other units
     * @param toAddList units to add
     * @return true if cycle would be created, false if not
     */
    boolean checkForCycle(Unit toAddTo, List<Unit> toAddList);
    
    /**
     * Removes components, that are not used by other assemblies from the system
     * 
     * @param componentsToCheck List of components which need to be checked
     * @param inventory List of all assemblies/components that exist in the system
     */
    void removeUnusedComponents(List<Unit> componentsToCheck, List<Unit> inventory);
}
