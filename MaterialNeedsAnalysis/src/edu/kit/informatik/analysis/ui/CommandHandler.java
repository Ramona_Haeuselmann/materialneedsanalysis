/**
 * 
 */
package edu.kit.informatik.analysis.ui;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.analysis.BomManagement;

/**
 * Entry point for material needs analysis system.
 * Handles user interaction
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class CommandHandler {

    /**
     * Handles user interaction for material needs analysis system
     * 
     * @param args all parameters passed will be ignored
     */
    public static void main(String[] args) {
        BomManagement bomMan = new BomManagement(); //bom management object to perform actions on
        boolean quit = false; //signals if user entered quit command
        Command command = null; //input command
        
        do {
            try {
                //try to match user input to a command
                command = Command.matchAndExecute(Terminal.readLine(), bomMan);
                if (command.isQuit()) {
                    quit = true; //exit program if user inputs quit command
                }
            } catch (InputException e) {
                Terminal.printError(e.getMessage()); //print error messages to the user
            }
        } while (command == null || !quit); 
    } 
}
