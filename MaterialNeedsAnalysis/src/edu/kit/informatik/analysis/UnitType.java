/**
 * 
 */
package edu.kit.informatik.analysis;

/**
 * Represents a type of a unit
 * Can be ASSEMBLY, COMPONENT or EITHER if it doesn't matter if assembly or component
 * 
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public enum UnitType {
    /**
     * Represents the type assembly
     */
    ASSEMBLY("ASSEMBLY"),
    
    /**
     * Represents the type component
     */
    COMPONENT("COMPONENT"),
    
    /**
     * Can be used if it doesn't matter if something is a component or assembly
     */
    EITHER("EITHER");
    
    private final String representation; //display text
    
    /*
     * Constructor for UnitType
     */
    private UnitType(String rep) {
        this.representation = rep;
    }
    
    @Override
    public String toString() {
        return this.representation;
    }
}
