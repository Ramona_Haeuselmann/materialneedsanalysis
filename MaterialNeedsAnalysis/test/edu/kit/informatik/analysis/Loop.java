package edu.kit.informatik.analysis;

import edu.kit.informatik.Terminal;
import edu.kit.informatik.analysis.ui.Command;
import edu.kit.informatik.analysis.ui.InputException;

public class Loop {

    /**
     * 
     * @param bomMan b
     */
    public static void loop(BomManagement bomMan) {
        try {
            //try to match user input to a command
            Command.matchAndExecute(Terminal.readLine(), bomMan);
        } catch (InputException e) {
            Terminal.printError(e.getMessage()); //print error messages to the user
        }
    }
}
