/**
 * 
 */
package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class TestAddPart {
    private BomManagement bomMan = new BomManagement();
    /**
     * @throws java.lang.Exception v
     */
    @Before
    public void setUp() throws Exception {
        bomMan = new BomManagement();
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * @throws java.lang.Exception v
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("addPart A+2:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addAssembly A=10:B;10:C");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addPart B+2:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart C+2:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        
        Terminal.writeInputBuffer("addPart A+2:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10"));
        
        Terminal.writeInputBuffer("printAssembly Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10"));
        
        Terminal.writeInputBuffer("addPart A+2:Z");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10;Z:2"));
        
        Terminal.writeInputBuffer("printAssembly Z");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("COMPONENT"));
        
        Terminal.writeInputBuffer("addPart A+998:Z");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10;Z:1000"));
        
        Terminal.writeInputBuffer("addPart A+1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("printAssembly A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:12;C:10;Z:1000"));
    }

}
