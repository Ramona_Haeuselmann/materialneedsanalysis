/**
 * 
 */
package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

/**
 * @author Ramona Haeuselmann
 * @version 1.0
 */
public class TestGetAssemblies {
    private BomManagement bomMan = new BomManagement();

    /**
     * @throws java.lang.Exception v
     */
    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
        Terminal.printToConsoleInTestMode = true;
    }

    /**
     * @throws java.lang.Exception v
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("addAssembly A=10:C;10:B");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("EMPTY"));
        
        Terminal.writeInputBuffer("getAssemblies B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals(""));
        
        Terminal.writeInputBuffer("getAssemblies C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
//        assertTrue(Terminal.readOutputBuffer().equals(""));
        
        Terminal.writeInputBuffer("addAssembly B=1:D;2:E");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("B:10"));
        
        Terminal.writeInputBuffer("addAssembly E=1:H;1:G");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("E:20;B:10"));
        
        Terminal.writeInputBuffer("addAssembly C=2:E;1:F");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getAssemblies A");
        Loop.loop(bomMan);
//        assertTrue(Terminal.readOutputBuffer().startsWith("Error, "));
        assertTrue(Terminal.readOutputBuffer().equals("E:40;B:10;C:10"));

    }

}
