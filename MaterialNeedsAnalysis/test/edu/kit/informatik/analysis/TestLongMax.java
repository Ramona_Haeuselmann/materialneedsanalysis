package edu.kit.informatik.analysis;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.kit.informatik.Terminal;

public class TestLongMax {
    private BomManagement bomMan = new BomManagement();
    
    /**
     * 
     * @throws Exception c
     */
    @Before
    public void setUp() throws Exception {
        Terminal.testMode = true;
    }

    /**
     * 
     * @throws Exception v
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * 
     */
    @Test
    public void test() {
        Terminal.writeInputBuffer("addAssembly A=256:B");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly B=256:C");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly C=256:D");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly D=256:E");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly E=256:F");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly F=256:G");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly G=256:H");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("addAssembly H=127:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("Z:9151314442816847872"));
    
        Terminal.writeInputBuffer("addPart H+1:Z");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("OK"));
        
        Terminal.writeInputBuffer("getComponents A");
        Loop.loop(bomMan);
        assertTrue(Terminal.readOutputBuffer().equals("Z:-9223372036854775808"));
    }

}
